﻿namespace ACI_L3OUT_Check
{
    partial class Form_Setting
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.Auditlog_Box = new System.Windows.Forms.CheckBox();
            this.AutoFix_LimitIP_Box = new System.Windows.Forms.CheckBox();
            this.UPDATE_button = new System.Windows.Forms.Button();
            this.MAX_RETRY_Box = new System.Windows.Forms.TextBox();
            this.MAX_RETRY_label = new System.Windows.Forms.Label();
            this.apicIP_Box = new System.Windows.Forms.TextBox();
            this.line_token_label = new System.Windows.Forms.Label();
            this.apic_id_Box = new System.Windows.Forms.TextBox();
            this.apic_pw_label = new System.Windows.Forms.Label();
            this.apic_pw_Box = new System.Windows.Forms.TextBox();
            this.apic_id_label = new System.Windows.Forms.Label();
            this.line_token_Box = new System.Windows.Forms.TextBox();
            this.apicIP_label = new System.Windows.Forms.Label();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.Auditlog_Box);
            this.groupBox1.Controls.Add(this.AutoFix_LimitIP_Box);
            this.groupBox1.Controls.Add(this.UPDATE_button);
            this.groupBox1.Controls.Add(this.MAX_RETRY_Box);
            this.groupBox1.Controls.Add(this.MAX_RETRY_label);
            this.groupBox1.Controls.Add(this.apicIP_Box);
            this.groupBox1.Controls.Add(this.line_token_label);
            this.groupBox1.Controls.Add(this.apic_id_Box);
            this.groupBox1.Controls.Add(this.apic_pw_label);
            this.groupBox1.Controls.Add(this.apic_pw_Box);
            this.groupBox1.Controls.Add(this.apic_id_label);
            this.groupBox1.Controls.Add(this.line_token_Box);
            this.groupBox1.Controls.Add(this.apicIP_label);
            this.groupBox1.Location = new System.Drawing.Point(12, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(460, 287);
            this.groupBox1.TabIndex = 11;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "SETTING";
            // 
            // Auditlog_Box
            // 
            this.Auditlog_Box.AutoSize = true;
            this.Auditlog_Box.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F);
            this.Auditlog_Box.Location = new System.Drawing.Point(17, 219);
            this.Auditlog_Box.Name = "Auditlog_Box";
            this.Auditlog_Box.Size = new System.Drawing.Size(187, 29);
            this.Auditlog_Box.TabIndex = 12;
            this.Auditlog_Box.Text = "Audit LOG Read";
            this.Auditlog_Box.UseVisualStyleBackColor = true;
            // 
            // AutoFix_LimitIP_Box
            // 
            this.AutoFix_LimitIP_Box.AutoSize = true;
            this.AutoFix_LimitIP_Box.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F);
            this.AutoFix_LimitIP_Box.Location = new System.Drawing.Point(17, 252);
            this.AutoFix_LimitIP_Box.Name = "AutoFix_LimitIP_Box";
            this.AutoFix_LimitIP_Box.Size = new System.Drawing.Size(174, 29);
            this.AutoFix_LimitIP_Box.TabIndex = 11;
            this.AutoFix_LimitIP_Box.Text = "AutoFix LimitIP";
            this.AutoFix_LimitIP_Box.UseVisualStyleBackColor = true;
            // 
            // UPDATE_button
            // 
            this.UPDATE_button.Location = new System.Drawing.Point(220, 219);
            this.UPDATE_button.Name = "UPDATE_button";
            this.UPDATE_button.Size = new System.Drawing.Size(228, 62);
            this.UPDATE_button.TabIndex = 10;
            this.UPDATE_button.Text = "UPDATE";
            this.UPDATE_button.UseVisualStyleBackColor = true;
            this.UPDATE_button.Click += new System.EventHandler(this.UPDATE_button_Click);
            // 
            // MAX_RETRY_Box
            // 
            this.MAX_RETRY_Box.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MAX_RETRY_Box.Location = new System.Drawing.Point(160, 167);
            this.MAX_RETRY_Box.Name = "MAX_RETRY_Box";
            this.MAX_RETRY_Box.Size = new System.Drawing.Size(288, 31);
            this.MAX_RETRY_Box.TabIndex = 4;
            // 
            // MAX_RETRY_label
            // 
            this.MAX_RETRY_label.AutoSize = true;
            this.MAX_RETRY_label.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MAX_RETRY_label.Location = new System.Drawing.Point(12, 170);
            this.MAX_RETRY_label.Name = "MAX_RETRY_label";
            this.MAX_RETRY_label.Size = new System.Drawing.Size(142, 25);
            this.MAX_RETRY_label.TabIndex = 9;
            this.MAX_RETRY_label.Text = "MAX_RETRY";
            // 
            // apicIP_Box
            // 
            this.apicIP_Box.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.apicIP_Box.Location = new System.Drawing.Point(160, 19);
            this.apicIP_Box.Name = "apicIP_Box";
            this.apicIP_Box.Size = new System.Drawing.Size(288, 31);
            this.apicIP_Box.TabIndex = 0;
            // 
            // line_token_label
            // 
            this.line_token_label.AutoSize = true;
            this.line_token_label.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.line_token_label.Location = new System.Drawing.Point(12, 133);
            this.line_token_label.Name = "line_token_label";
            this.line_token_label.Size = new System.Drawing.Size(111, 25);
            this.line_token_label.TabIndex = 8;
            this.line_token_label.Text = "line_token";
            // 
            // apic_id_Box
            // 
            this.apic_id_Box.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.apic_id_Box.Location = new System.Drawing.Point(160, 56);
            this.apic_id_Box.Name = "apic_id_Box";
            this.apic_id_Box.Size = new System.Drawing.Size(288, 31);
            this.apic_id_Box.TabIndex = 1;
            // 
            // apic_pw_label
            // 
            this.apic_pw_label.AutoSize = true;
            this.apic_pw_label.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.apic_pw_label.Location = new System.Drawing.Point(12, 96);
            this.apic_pw_label.Name = "apic_pw_label";
            this.apic_pw_label.Size = new System.Drawing.Size(91, 25);
            this.apic_pw_label.TabIndex = 7;
            this.apic_pw_label.Text = "apic_pw";
            // 
            // apic_pw_Box
            // 
            this.apic_pw_Box.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.apic_pw_Box.Location = new System.Drawing.Point(160, 93);
            this.apic_pw_Box.Name = "apic_pw_Box";
            this.apic_pw_Box.Size = new System.Drawing.Size(288, 31);
            this.apic_pw_Box.TabIndex = 2;
            this.apic_pw_Box.UseSystemPasswordChar = true;
            // 
            // apic_id_label
            // 
            this.apic_id_label.AutoSize = true;
            this.apic_id_label.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.apic_id_label.Location = new System.Drawing.Point(12, 59);
            this.apic_id_label.Name = "apic_id_label";
            this.apic_id_label.Size = new System.Drawing.Size(81, 25);
            this.apic_id_label.TabIndex = 6;
            this.apic_id_label.Text = "apic_id";
            // 
            // line_token_Box
            // 
            this.line_token_Box.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.line_token_Box.Location = new System.Drawing.Point(160, 130);
            this.line_token_Box.Name = "line_token_Box";
            this.line_token_Box.Size = new System.Drawing.Size(288, 31);
            this.line_token_Box.TabIndex = 3;
            // 
            // apicIP_label
            // 
            this.apicIP_label.AutoSize = true;
            this.apicIP_label.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.apicIP_label.Location = new System.Drawing.Point(12, 22);
            this.apicIP_label.Name = "apicIP_label";
            this.apicIP_label.Size = new System.Drawing.Size(71, 25);
            this.apicIP_label.TabIndex = 5;
            this.apicIP_label.Text = "apicIP";
            // 
            // Form_Setting
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(484, 311);
            this.Controls.Add(this.groupBox1);
            this.Name = "Form_Setting";
            this.Text = "Setting_Form";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button UPDATE_button;
        private System.Windows.Forms.TextBox MAX_RETRY_Box;
        private System.Windows.Forms.Label MAX_RETRY_label;
        private System.Windows.Forms.TextBox apicIP_Box;
        private System.Windows.Forms.Label line_token_label;
        private System.Windows.Forms.TextBox apic_id_Box;
        private System.Windows.Forms.Label apic_pw_label;
        private System.Windows.Forms.TextBox apic_pw_Box;
        private System.Windows.Forms.Label apic_id_label;
        private System.Windows.Forms.TextBox line_token_Box;
        private System.Windows.Forms.Label apicIP_label;
        private System.Windows.Forms.CheckBox AutoFix_LimitIP_Box;
        private System.Windows.Forms.CheckBox Auditlog_Box;
    }
}