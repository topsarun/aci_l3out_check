﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Configuration;
using System.Collections.Specialized;

using RestSharp;
using Newtonsoft;
using Newtonsoft.Json.Linq;
using System.Net;

namespace ACI_L3OUT_Check
{
    public partial class Form_Main : Form
    {

        private void ReadSetting()
        {
            apicIP = ConfigurationManager.AppSettings.Get("APIC-1_IP");
            apic_id = ConfigurationManager.AppSettings.Get("APIC_ID");
            apic_pw = ConfigurationManager.AppSettings.Get("APIC_PASS");
            line_token = ConfigurationManager.AppSettings.Get("Line_token");
            MAX_RETRY = int.Parse(ConfigurationManager.AppSettings.Get("MAX_RETRY"));
            Audit_Log_Read = int.Parse(ConfigurationManager.AppSettings.Get("Audit_Log_Read"));
            AutoFix_LimitIP = int.Parse(ConfigurationManager.AppSettings.Get("AutoFix_LimitIP"));
        }


        string apicIP;
        string apic_id;
        string apic_pw;
        string line_token;
        int MAX_RETRY;
        int Audit_Log_Read;
        int AutoFix_LimitIP;

        int Login_Retry = 0;
        int BD_Retry = 0;
        int Audit_log_Retry = 0;

        string[,] TN_list = new string[500,500];
        int TN_Count = 0;

        RestClient client = new RestClient();
        
        public Form_Main()
        {
            InitializeComponent();
            client.Timeout = 500;
            ReadSetting();
        }

        private Boolean Login_API(string username, string password)
        {
            string sessionId = "", payload = "";
            RestRequest login_post;
            IRestResponse login_post_response;
            JObject login_data;

            client.BaseUrl = new System.Uri("https://" + apicIP + "/api/aaaLogin.json");
            client.CookieContainer = new System.Net.CookieContainer();
            ServicePointManager.ServerCertificateValidationCallback += (RestClient, certificate, chain, sslPolicyErrors) => true;

            payload = "payload{\"aaaUser\":{\"attributes\":{\"name\":\"" + username + "\", \"pwd\":\"" + password + "\"}}}";

            login_post = new RestRequest(Method.POST);
            login_post.AddHeader("content-type", "application/json");
            login_post.AddParameter("application/json", payload, ParameterType.RequestBody);

            try
            {
                login_post_response = client.Execute(login_post);
                login_data = JObject.Parse(login_post_response.Content);
                sessionId = (login_data["imdata"][0]["aaaLogin"]["attributes"]["sessionId"].ToString());;

                Status_bar.Text = sessionId;
            }
            catch
            {
                return false;
            }
            return true; // no error
        }

        private void TN_timer_Tick(object sender, EventArgs e)
        {
            ReadSetting();
            while (Login_API(apic_id, apic_pw) == false && Login_Retry <= MAX_RETRY)
            {
                Login_Retry++;
                Status_bar.Text = "[L3OUT]Can't connect to " + apicIP + " #" + Login_Retry;
            }
            if (Login_Retry > MAX_RETRY)
            {
                LineNotify("[L3OUT]Can't connect to " + apicIP);
                Login_Retry = 0;
            }
            else
            {
                Login_Retry = 0;
            }

            TN_Listbox.Text = "";

            RestRequest request = new RestRequest();
            client.BaseUrl = new System.Uri("https://" + apicIP + "/api/node/class/fvTenant.json");
            request = new RestRequest(Method.GET);
            ServicePointManager.ServerCertificateValidationCallback += (RestRequest, certificate, chain, sslPolicyErrors) => true;
            request.AddHeader("cache-control", "no-cache");
            
            try
            {
                IRestResponse response1 = client.Execute(request);

                JObject datastat = JObject.Parse(response1.Content);
                TN_Count = int.Parse((datastat["totalCount"].ToString()));

                TN_Listbox.Text += "Total TN = " + TN_Count.ToString() + "\r\n";

                for (int i = 0; i < TN_Count; i++)
                {
                    if (i % 10 == 0 && i != 0)
                        Login_API(apic_id, apic_pw);

                    TN_Progress_Bar.Value = (int)Math.Floor(((i + 1.0) / TN_Count) * 100);
                    TN_Listbox.Text += (i+1).ToString() + " ";
                    TN_list[i,0] = datastat["imdata"][i]["fvTenant"]["attributes"]["dn"].ToString();
                    TN_Listbox.Text += TN_list[i,0] +"\r\n";
                    while (LIST_BD(TN_list[i, 0], i) == false && BD_Retry <= MAX_RETRY)
                    {
                        BD_Retry++;
                    }
                    if(BD_Retry > MAX_RETRY)
                    {
                        //LineNotify("[L3OUT]Can't connect get BD in TN " + TN_list[i, 0] );
                        BD_Retry = 0;
                    }
                    else
                    {
                        BD_Retry = 0;
                    }
                }

                button1.BackColor = Color.Green;
                DateTime localDate = DateTime.Now;
                Status_bar.Text = "Run success@" + localDate.ToString() ;
            }
            catch
            {
                button1.BackColor = Color.Red;
                return;
            }
        }

        private Boolean LIST_BD(string TN, int ROW)
        {
            int BD_Count;
            string limitIp_Filed;

            RestRequest request = new RestRequest();
            client.BaseUrl = new System.Uri("https://" + apicIP + "/api/node/mo/" + TN + ".json?query-target=children&target-subtree-class=fvBD&order-by=fvBD.name");
            request = new RestRequest(Method.GET);
            ServicePointManager.ServerCertificateValidationCallback += (RestRequest, certificate, chain, sslPolicyErrors) => true;
            request.AddHeader("cache-control", "no-cache");
            IRestResponse response1 = client.Execute(request);

            JObject datastat = JObject.Parse(response1.Content);
            BD_Count = int.Parse((datastat["totalCount"].ToString()));
            
            try
            {
                for (int i = 0; i < BD_Count; i++)
                {
                    TN_Listbox.Text += " BD: " + (i + 1).ToString() + " ";
                    //MessageBox.Show(datastat["imdata"][i]["fvBD"]["attributes"]["name"].ToString());
                    TN_list[ROW, i + 1] = datastat["imdata"][i]["fvBD"]["attributes"]["name"].ToString();
                    TN_Listbox.Text += TN_list[ROW, i + 1] + "\r\n";

                    bool check_word = TN_list[ROW, i + 1].Contains("BD_L3OUT");
                    if(check_word)
                    {
                        limitIp_Filed = datastat["imdata"][i]["fvBD"]["attributes"]["limitIpLearnToSubnets"].ToString();
                        if (limitIp_Filed == "no")
                        {
                            LineNotify(TN_list[ROW, i + 1] + " No Config LimitIptoSubnet");

                            if (Audit_Log_Read == 1)
                            {
                                //READ AUDIT LOG and Line Notify
                                while (Audit_Log_Check(TN, TN_list[ROW, i + 1]) == false && Audit_log_Retry <= MAX_RETRY)
                                {
                                    Audit_log_Retry++;
                                }
                                if (Audit_log_Retry > MAX_RETRY)
                                {
                                    LineNotify("[L3OUT]Can't connect get Log in BD " + TN_list[ROW, i + 1]);
                                    Audit_log_Retry = 0;
                                }
                                else
                                {
                                    Audit_log_Retry = 0;
                                }
                            }
                            
                            if (AutoFix_LimitIP == 1)
                            {
                                //AUTO FIX limitIpLearnToSubnets
                                AutoFix_LimitIpLearnToSubnets(TN, TN_list[ROW, i + 1]);
                            }
                        }
                    }                   
                }
            }
            catch
            {
                return false;
            }
            return true;
        }

        private Boolean Audit_Log_Check(string Name_TN, string Name_BD)
        {
            string user;

            RestRequest request = new RestRequest();
            client.BaseUrl = new System.Uri("https://" + apicIP + "/api/node/mo/" + Name_TN + "/BD-" + Name_BD + ".json?rsp-subtree-include=audit-logs,no-scoped,subtree&order-by=aaaModLR.created");
            request = new RestRequest(Method.GET);
            ServicePointManager.ServerCertificateValidationCallback += (RestRequest, certificate, chain, sslPolicyErrors) => true;
            request.AddHeader("cache-control", "no-cache");
            IRestResponse response1 = client.Execute(request);

            try
            {
                JObject datastat = JObject.Parse(response1.Content);
                user = datastat["imdata"][0]["aaaModLR"]["attributes"]["user"].ToString();
                LineNotify("ID " + user + " Creat " + Name_BD);
                if(user !="sarun" && user != "panthep.pr")
                    Samuttchai_API("[ACI]ID " + user + " Creat " + Name_BD + " Config ACI ไม่ครบ");
                return true;
            }
            catch
            {
                return false;
            }
        }

        private Boolean AutoFix_LimitIpLearnToSubnets(string Name_TN,string Name_BD)
        {
            RestRequest request;
            IRestResponse response;
            string Input;

            client.BaseUrl = new System.Uri("https://" + apicIP + "/api/node/mo/" + Name_TN + "/BD-" + Name_BD + ".json");
            ServicePointManager.ServerCertificateValidationCallback += (RestClient, certificate, chain, sslPolicyErrors) => true;
            Input = "payload{\"fvBD\":{\"attributes\":{\"dn\":\"" + Name_TN +  "/BD-" + Name_BD + "\",\"limitIpLearnToSubnets\":\"true\"},\"children\":[]}}";
            
            request = new RestRequest(Method.POST);
            request.AddHeader("content-type", "application/json");
            request.AddParameter("application/json", Input, ParameterType.RequestBody);

            try
            {
                response = client.Execute(request);
                JObject datastat = JObject.Parse(response.Content);             
                if (int.Parse((datastat["totalCount"].ToString())) == 0)
                {
                    LineNotify("AutoFix " + Name_BD + " Success");
                    return true;
                }
                else
                {
                    LineNotify("AutoFix " + Name_BD + " Failed");
                }
            }
            catch
            {
                LineNotify("AutoFix " + Name_BD + " Failed");
            }
            return false;
        }

        private void TN_Listbox_TextChanged(object sender, EventArgs e)
        {
            TN_Listbox.SelectionStart = TN_Listbox.Text.Length;
            TN_Listbox.ScrollToCaret();
        }

        #region<LINE>
        private int LineNotify(string msg)
        {
            RestRequest line_post;
            RestClient line_client = new RestClient
            {
                BaseUrl = new System.Uri("https://notify-api.line.me/api/notify")
            };
            ServicePointManager.ServerCertificateValidationCallback += (RestClient, certificate, chain, sslPolicyErrors) => true;

            line_post = new RestRequest(Method.POST); ;
            line_post.AddHeader("Authorization", string.Format("Bearer " + line_token));
            line_post.AddHeader("content-type", "application/x-www-form-urlencoded");
            line_post.AddParameter("message", msg);

            try
            {
                IRestResponse response = line_client.Execute(line_post);
                return int.Parse((JObject.Parse(response.Content)["status"].ToString()));
            }
            catch
            {
                Status_bar.Text = "Can't connect to line server !!";
                MessageBox.Show("Can't connect to line server !!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return 1;
            }
        }
        #endregion

        private void ManualToolStripMenuItem_Click(object sender, EventArgs e)
        {
            TN_timer_Tick(null, null);
        }

        private void SettingToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Form_Setting Frm_Setting = new Form_Setting();
            Frm_Setting.ShowDialog();
        }

        private void ExitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void Samuttchai_API(string msg)
        {
            RestRequest Samuttchai_post;
            RestClient Samuttchai_client = new RestClient();

            Samuttchai_client.BaseUrl = new System.Uri("http://203.150.221.195/monitor/alert-line.php");
            ServicePointManager.ServerCertificateValidationCallback += (RestClient, certificate, chain, sslPolicyErrors) => true;

            Samuttchai_post = new RestRequest(Method.POST); ;
            Samuttchai_post.AddHeader("content-type", "application/x-www-form-urlencoded");
            Samuttchai_post.AddParameter("group", "alert-incident");
            Samuttchai_post.AddParameter("msg", msg);

            IRestResponse response = Samuttchai_client.Execute(Samuttchai_post);
        }

        private void tESTAPIToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Samuttchai_API("เทสโปรแกรมจ่ะ");
        }
    }
}
