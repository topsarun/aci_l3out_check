﻿using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

// General Information about an assembly is controlled through the following
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle("ACI BD-L3OUT Check")]
[assembly: AssemblyDescription("Check BD-L3OUT to config LimitIPtoSubnet")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("INET BD-NETWORK")]
[assembly: AssemblyProduct("ACI BD-L3OUT Check")]
[assembly: AssemblyCopyright("Copyright Sarun.An@inet.co.th ©  2018")]
[assembly: AssemblyTrademark("INET BD-NETWORK")]
[assembly: AssemblyCulture("")]

// Setting ComVisible to false makes the types in this assembly not visible
// to COM components.  If you need to access a type in this assembly from
// COM, set the ComVisible attribute to true on that type.
[assembly: ComVisible(false)]

// The following GUID is for the ID of the typelib if this project is exposed to COM
[assembly: Guid("4744fcd3-2364-4367-99b4-25ccb7660d95")]

// Version information for an assembly consists of the following four values:
//
//      Major Version
//      Minor Version
//      Build Number
//      Revision
//
// You can specify all the values or you can default the Build and Revision Numbers
// by using the '*' as shown below:
// [assembly: AssemblyVersion("1.0.*")]
[assembly: AssemblyVersion("1.0.0.0")]
[assembly: AssemblyFileVersion("1.0.0.0")]
