﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Configuration;
using System.Collections.Specialized;

namespace ACI_L3OUT_Check
{
    public partial class Form_Setting : Form
    {
        string apicIP = ConfigurationManager.AppSettings.Get("APIC-1_IP");
        string apic_id = ConfigurationManager.AppSettings.Get("APIC_ID");
        string apic_pw = ConfigurationManager.AppSettings.Get("APIC_PASS");
        string line_token = ConfigurationManager.AppSettings.Get("Line_token");
        int MAX_RETRY = int.Parse(ConfigurationManager.AppSettings.Get("MAX_RETRY"));
        int Audit_Log_Read = int.Parse(ConfigurationManager.AppSettings.Get("Audit_Log_Read"));
        int AutoFix_LimitIP = int.Parse(ConfigurationManager.AppSettings.Get("AutoFix_LimitIP"));

        public Form_Setting()
        {
            InitializeComponent();
            apicIP_Box.Text = apicIP;
            apic_id_Box.Text = apic_id;
            apic_pw_Box.Text = apic_pw;
            line_token_Box.Text = line_token;
            MAX_RETRY_Box.Text = MAX_RETRY.ToString();
            if (Audit_Log_Read == 1) Auditlog_Box.CheckState = CheckState.Checked;
            if (AutoFix_LimitIP == 1) AutoFix_LimitIP_Box.CheckState = CheckState.Checked;
        }

        private void UPDATE_button_Click(object sender, EventArgs e)
        {
            var confirmResult = MessageBox.Show("Are you sure ??", "Confirm !!", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);

            if (confirmResult == DialogResult.Yes)
            {
                Configuration config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);

                config.AppSettings.Settings["APIC-1_IP"].Value = apicIP_Box.Text;
                config.AppSettings.Settings["APIC_ID"].Value = apic_id_Box.Text;
                config.AppSettings.Settings["APIC_PASS"].Value = apic_pw_Box.Text;
                config.AppSettings.Settings["Line_token"].Value = line_token_Box.Text;
                config.AppSettings.Settings["MAX_RETRY"].Value = MAX_RETRY_Box.Text;

                if (Auditlog_Box.CheckState == CheckState.Checked)
                    config.AppSettings.Settings["Audit_Log_Read"].Value = "1";
                else
                    config.AppSettings.Settings["Audit_Log_Read"].Value = "0";

                if (AutoFix_LimitIP_Box.CheckState == CheckState.Checked)
                    config.AppSettings.Settings["AutoFix_LimitIP"].Value = "1";
                else
                    config.AppSettings.Settings["AutoFix_LimitIP"].Value = "0";

                config.Save(ConfigurationSaveMode.Modified);
                ConfigurationManager.RefreshSection("appSettings");
                MessageBox.Show("OK");
            }
        }
    }
}
